package com.tw.bronze.question2;

import java.util.HashMap;
import java.util.List;

public class ReceiptPrinter {
    private final HashMap<String, Product> products =
        new HashMap<>();

    public ReceiptPrinter(List<Product> products) {
        // TODO:
        //   Please implement the constructor
        // <-start-
        for (int i = 0; i < products.size(); i++) {
            this.products.put(products.get(i).getId(), products.get(i));
        }
        // --end->
    }

    public int getTotalPrice(List<String> selectedIds) {
        // TODO:
        //   Please implement the method
        // <-start-
        try {
            int sumPrice = 0;
            for (int i = 0; i < selectedIds.size(); i++) {
                sumPrice += products.get(selectedIds.get(i)).getPrice();
            }
            return sumPrice;
        } catch (Exception e) {
            throw new IllegalArgumentException("Please enter a valid list.");
        }
        // --end->
    }
}

